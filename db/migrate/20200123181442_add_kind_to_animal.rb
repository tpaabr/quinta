class AddKindToAnimal < ActiveRecord::Migration[5.1]
  def change
    add_column :animals, :kind, :integer
  end
end
