class CreateAnimalFoods < ActiveRecord::Migration[5.1]
  def change
    create_table :animal_foods do |t|
      t.references :animal, foreign_key: true
      t.references :food, foreign_key: true

      t.timestamps
    end
  end
end
