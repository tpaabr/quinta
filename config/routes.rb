Rails.application.routes.draw do
  resources :animal_foods
  resources :territories
  resources :foods
  resources :animals
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
