class Food < ApplicationRecord

    has_many :animal_food
    has_many :food, through: :animal_food

end
