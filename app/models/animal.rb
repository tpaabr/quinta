class Animal < ApplicationRecord



    has_many :animal_food
    has_many :food, through: :animal_food

    enum kind: {
        Vegetariano: 0,
        Carnivoro: 1,
    }
end
