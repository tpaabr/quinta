class AnimalFood < ApplicationRecord
  belongs_to :animal
  belongs_to :food
end
