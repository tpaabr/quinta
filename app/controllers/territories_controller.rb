class TerritoriesController < ApplicationController
  before_action :set_territory, only: [:show, :update, :destroy]

  # GET /territories
  def index
    @territories = Territory.all

    render json: @territories
  end

  # GET /territories/1
  def show
    render json: @territory
  end

  # POST /territories
  def create
    @territory = Territory.new(territory_params)

    if @territory.save
      render json: @territory, status: :created, location: @territory
    else
      render json: @territory.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /territories/1
  def update
    if @territory.update(territory_params)
      render json: @territory
    else
      render json: @territory.errors, status: :unprocessable_entity
    end
  end

  # DELETE /territories/1
  def destroy
    @territory.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_territory
      @territory = Territory.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def territory_params
      params.require(:territory).permit(:name, :size)
    end
end
